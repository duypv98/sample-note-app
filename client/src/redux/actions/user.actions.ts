import { userActions } from '../../constants/actions';

export const actLoginSuccess = (token: string) => ({
  type: userActions.LOGIN_SUCCESS,
  payload: { token }
});

export const actLogout = () => ({
  type: userActions.SET_LOGOUT,
  payload: {}
});

export const actLoadInfo = (fullName: string, email: string, phone: string) => ({
  type: userActions.LOAD_INFO,
  payload: { fullName, email, phone }
});

export const actLogin = (email: string, password: string) => ({
  type: userActions.LOGIN,
  payload: { email, password }
});

export const actSetLoading = (isLoading: boolean) => ({
  type: userActions.SET_USER_LOADING,
  payload: { isLoading }
});
