import { noteActions } from '../../constants/actions';

export const actAddNote = (id: string, content: string) => ({
  type: noteActions.ADD_NOTE,
  payload: { id, content }
});

export const actEditNote = (id: string, content: string) => ({
  type: noteActions.EDIT_NOTE,
  payload: { id, content }
});

export const actRemoveNote = (id: string) => ({
  type: noteActions.REMOVE_NOTE,
  payload: { id }
});

export const actSetDraft = (id: string, draft: string) => ({
  type: noteActions.SET_DRAFT,
  payload: { id, draft }
});

export const actFetchNotes = (notes: any) => ({
  type: noteActions.FETCH_NOTES,
  payload: { notes }
});
