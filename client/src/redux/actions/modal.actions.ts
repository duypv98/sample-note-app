import { modalActions } from '../../constants/actions';

export const actToggleAlert = (isShow: boolean, message?: string) => ({
  type: modalActions.TOGGLE,
  payload: { isShow, message }
});

export const actToggleRedirect = (isShow: boolean) => ({
  type: modalActions.TOGGLE_REDIRECT,
  payload: { isShow }
});
