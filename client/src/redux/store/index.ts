import { createStore, applyMiddleware } from 'redux';
import reduxSaga from 'redux-saga';

import reducers from '../reducers';
import rootSaga from '../sagas';

const makeStore = () => {
  const sagaMiddleware = reduxSaga();
  const store = createStore(reducers, applyMiddleware(sagaMiddleware));

  sagaMiddleware.run(rootSaga);
  return store;
};

export default makeStore;
