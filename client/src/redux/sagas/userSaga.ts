import {
  all, call, put, takeLatest
} from 'redux-saga/effects';

import { actLoginSuccess, actSetLoading } from '../actions/user.actions';
import { post } from '../../utils/request';
import { actToggleAlert } from '../actions/modal.actions';
import { userActions } from '../../constants/actions';
import { IAction } from '../types';

function* login(action: IAction) {
  const { email, password } = action.payload;
  yield put(actSetLoading(true));
  const { error, ...response } = yield call(post, '/auth/login', { email, password }, null);
  if (error) {
    yield put(actToggleAlert(true, response.message));
  } else {
    yield put(actLoginSuccess(response.data.token));
  }
}

function* watchLogin() {
  yield takeLatest(userActions.LOGIN, login);
}

export default function* userSaga() {
  yield all([
    watchLogin()
  ]);
}
