export const userActions = {
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  SET_LOGOUT: 'SET_LOGOUT',
  LOAD_INFO: 'LOAD_INFO',
  SET_USER_LOADING: 'SET_USER_LOADING',
  // saga
  LOGIN: 'LOGIN',
};

export const noteActions = {
  ADD_NOTE: 'ADD_NOTE',
  REMOVE_NOTE: 'REMOVE_NOTE',
  EDIT_NOTE: 'EDIT_NOTE',
  SET_DRAFT: 'SET_DRAFT',
  FETCH_NOTES: 'FETCH_NOTES'
};

export const modalActions = {
  TOGGLE: 'TOGGLE',
  TOGGLE_REDIRECT: 'TOGGLE_REDIRECT'
};

export const anotherAction = {};
